package Rechteck;

//Klasse 
public class Rechteck {

	
	private double seitenlaengeA;
	private double seitenlaengeB;
    
	public Rechteck(double seitenlaengeA, double seitenlaengeB)
	{
	  setSeiteA(seitenlaengeA);
	  setSeiteB(seitenlaengeB);
	}
	
	public void setSeiteA(double seitenlaengeA) {
		if(seitenlaengeA > 0)
			this.seitenlaengeA = seitenlaengeA;
		else
			this.seitenlaengeA = 0;
	}
	
	public double getSeiteA() {
		return this.seitenlaengeA;
	}
	
	
	public void setSeiteB(double seitenlaengeB) {
		if(seitenlaengeB > 0)
			this.seitenlaengeB = seitenlaengeB;
		else
			this.seitenlaengeB = 0;
	}
	
	public double getSeiteB() {
		return this.seitenlaengeB;
	} 
	
	public double getFlaeche() {
		return this.seitenlaengeA * this.seitenlaengeB;
	}
	
	public double getUmfang() {
		return (2 * this.seitenlaengeA) + (2 * this.seitenlaengeB);
	}
	
	
}
