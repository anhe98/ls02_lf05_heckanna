package Raumschiff;

import Raumschiff.Raumschiff;

public class RaumschiffMain {

	public static void main(String[] args) {

		// Neue Objekte Raumschiff
		Raumschiff klingonen = new Raumschiff("IKS Hegh`ta", 1, 100, 100, 100, 100, 2);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 100, 2);
		Raumschiff vulkanier = new Raumschiff("Ni`Var", 0, 80, 80, 50, 100, 5);

		// Neue Objekte Ladung
		Ladung ladung1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ladung2 = new Ladung("Borg-Schrott", 5);
		Ladung ladung3 = new Ladung("Rote Materie", 2);
		Ladung ladung4 = new Ladung("Forschungssonde", 35);
		Ladung ladung5 = new Ladung("Bat`leth Klingonen Schwert", 200);
		Ladung ladung6 = new Ladung("Plasma-Waffe", 50);
		Ladung ladung7 = new Ladung("Photonentorpedo", 3);

		// Ladung zu den jeweiligen Raumschiffen hinzuf�gen
		klingonen.addLadung(ladung1);
		klingonen.addLadung(ladung5);
		romulaner.addLadung(ladung2);
		romulaner.addLadung(ladung3);
		romulaner.addLadung(ladung6);
		vulkanier.addLadung(ladung4);
		vulkanier.addLadung(ladung7);

		// Klingonen schie�en mit dem Photonentorpedo einmal auf die Romulaner
		klingonen.photonentorpedoSchiessen(romulaner);

		// Romulaner schie�en mit der Phaserkanone zur�ck
		romulaner.phaserkanoneSchiessen(klingonen);

		// Vulkanier senden eine Nachricht an Alle �Gewalt ist nicht logisch�
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");

		// Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();

		// Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein
		vulkanier.reparaturDurchfuehren(true, true, true, 5);

		// Vulkanier verladen Ihre Ladung �Photonentorpedos� in die Torpedor�hren Ihres Raumschiffes
		// und r�umen das Ladungsverzeichnis auf
		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAufraumen();

		// Klingonen schie�en mit zwei weiteren Photonentorpedo auf die Romulaner
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);

		// Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab
		// und geben Ihr Ladungsverzeichnis aus
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();

	}

}
