package Raumschiff;

import java.util.ArrayList;

public class Raumschiff {

	// Attribute der Klasse Raumschiff
	String schiffsname;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	// Konstruktor
	public Raumschiff() {
	}

	// Konstruktor mit Parametern
	public Raumschiff(String schiffsname, int photonentorpedoAnzahl, int energieversorgungInProzent,
			int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl) {
		this.schiffsname = schiffsname;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
	}

	// Schiffsname
	public void setSchiffsname(String schiffsname) {

		this.schiffsname = schiffsname;
	}

	public String getSchiffsname() {
		return this.schiffsname;
	}

	// Photonentorpedo
	public void setPhotonentorpedo(int photonentorpedoAnzahl) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getPhotonentorpedo() {
		return this.photonentorpedoAnzahl;
	}

	// Energieversorgung
	public void setEnergieversorgung(int energieversorgungInProzent) {

		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getEnergieversorgung() {
		return this.energieversorgungInProzent;
	}

	// Schilde
	public void setSchilde(int schildeInProzent) {

		this.schildeInProzent = schildeInProzent;
	}

	public int getSchilde() {
		return this.schildeInProzent;
	}

	// H�lle
	public void setHuelle(int huelleInProzent) {

		this.huelleInProzent = huelleInProzent;
	}

	public int getHuelle() {
		return this.huelleInProzent;
	}

	// Lebenserhaltungssysteme
	public void setLebenserhaltung(int lebenserhaltungssystemeInProzent) {

		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getLebenserhaltung() {
		return this.lebenserhaltungssystemeInProzent;
	}

	// Androidenanzahl
	public void setAndroidenanzahl(int androidenAnzahl) {

		this.androidenAnzahl = androidenAnzahl;
	}

	public int getAndroidenanzahl() {
		return this.androidenAnzahl;
	}

	/**
	* F�gt ein Objekt der Klasse Ladung zum Ladungsverzeichnis hinzu
	*
	* @param neueLadung Objekt der Klasse Ladung, das zum Ladungsverzeichnis hinzugef�gt werden soll
	* 
	*/
	public void addLadung(Ladung neueLadung) {

		ladungsverzeichnis.add(neueLadung);

	}

	/**
	* Pr�ft, ob Photonentorpedos geladen sind und �bergibt je Ergebnis eine bestimmte Nachricht an die Methode nachrichtAnAlle(). 
	* Sind Photonentorpedos geladen, wird die Anzahl um 1 reduziert und die Methode treffer() aufgerufen.
	*
	* @param r das getroffene Raumschiff
	* 
	*/
	public void photonentorpedoSchiessen(Raumschiff r) {

		if (photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(r);
		}
	}

	/**
	* Pr�ft, ob die Energieversorgung in Prozent unter 50 ist und gibt je nach Ergebnis eine bestimmte Nachricht aus. 
	* Ist die Energieversorgung �ber 50 wird diese um 50 reduziert und die Methode treffer() aufgerufen .
	*
	* @param r das getroffene Raumschiff
	* 
	*/
	public void phaserkanoneSchiessen(Raumschiff r) {

		if (energieversorgungInProzent <= 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			setEnergieversorgung(energieversorgungInProzent - 50);
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(r);
		}

	}

	/**
	* Verarbeitet den Treffer eines Raumschiffes. Die SchilInProzent werden um 50 reduziert.
	* Es wird gepr�ft, ob die Schilde kleiner gleich 0 sind. In diesem Fall werden die H�lle und die Lebenserhaltungssysteme auch um 50 reduziert.
	* Danach wird gepr�ft, ob die H�lle kleiner gleich sind. In diesem Fall werden die Lebenserhaltungssysteme vollst�ndig zerst�rt und eine Nachricht an Alle ausgegeben.
	* 
	* @param r das getroffene Raumschiff
	* 
	*/
	private void treffer(Raumschiff r) {

		System.out.println("\n" + r.schiffsname + " wurde getroffen. ");
		r.schildeInProzent = r.schildeInProzent - 50;
		if (r.schildeInProzent <= 0) {
			r.huelleInProzent = r.huelleInProzent - 50;
			r.lebenserhaltungssystemeInProzent = r.lebenserhaltungssystemeInProzent - 50;
		}
		if (r.huelleInProzent <= 0) {
			r.lebenserhaltungssystemeInProzent = 0;

			nachrichtAnAlle("\nLebenserhaltungssysteme wurden vernichtet!");
		}

	}

	/**
	* �bergibt den Parameter an den Broadcast Kommunikator und gibt diese auf der Konsole aus.
	*
	* @param message die auszugebene Nachricht
	* 
	*/
	public void nachrichtAnAlle(String message) {
		// Die Nachricht wird dem broadcastKommunikator hinzugef�gt
		broadcastKommunikator.add(message);
		// Nachricht wird ausgegeben
		System.out.println("\n* " + message + " *");
	}

	/**
	* �berpr�ft, wie viele Photonentorpedos das Raumschiff geladen hat. Sind keine verf�gbar, wird eine Nachricht an Alle ausgeben.
	* Sind weniger Torpedos als angegeben verf�gbar, wird dies auf der Konsole ausgegeben und nur die Azahl der verf�gbaren Torpedos geladen.
	* Sind gen�gen Torpedos geladen, wird dies auf der Konsole ausgegeben und die gew�nschte Anzahl geladen. Dabei wird die Ladung um diese Menge reduziert.
	*
	* @param anzahlTorpedos die Anzahl der Torpedos, die geladen werden sollen
	* 
	*/
	public void photonentorpedosLaden(int anzahlTorpedos) {

		String torpedoAbfrage;
		int mengeTorpedos;
		
		for (int counter = 1; counter <= ladungsverzeichnis.size(); counter++) {

			torpedoAbfrage = ladungsverzeichnis.get(counter - 1).getBezeichnung();
			if (torpedoAbfrage == "Photonentorpedo") {
				mengeTorpedos = ladungsverzeichnis.get(counter - 1).getMenge();

				// Pr�fen ob Photonentorpedos geladen sind oder nicht
				if (mengeTorpedos == 0) {
					System.out.println("\n " + schiffsname + " hat keine Photonentorpedos geladen");
					nachrichtAnAlle("-=*Click*=-");
				} else {
					if (anzahlTorpedos > mengeTorpedos) {
						System.out.println(
								"\n " + schiffsname + " hat nicht gen�gend Photonentorpedos auf Lager, es werden "
										+ mengeTorpedos + " Photonentorpedos eingesetzt.");
						setPhotonentorpedo(photonentorpedoAnzahl + mengeTorpedos);
						ladungsverzeichnis.get(counter - 1).setMenge(0);
					} else {
						System.out.println("\n " + schiffsname + " setzt " + anzahlTorpedos + " Photonentorpedos ein.");
						setPhotonentorpedo(photonentorpedoAnzahl + anzahlTorpedos);
						ladungsverzeichnis.get(counter - 1).setMenge(mengeTorpedos - anzahlTorpedos);
					}
				}
			}
		}
	}

	/**
	* Bestimmt die Anzahl der zu reparierenden Parameter und �berpr�ft, dass mindestens einer auf True gesetzt wurde. 
	* Berechnet die Prozentzahl, um die die gew�hlten Parameter eh�ht werden sollen und addiert den Wert zu den Attributwerten.
	*
	* @param schutzschilde Information ob Schutzschilde repariert werden sollen
	* @param energieversorgung Information ob die Energierversorgung repariert werden soll
	* @param schiffshuelle Information ob die H�lle repariert werden soll
	* @param anzahlDroiden die Anzahl der Droiden, die zur Reparatur eingesetzt werden sollen
	* 
	*/
	public void reparaturDurchfuehren(Boolean schutzschilde, Boolean energieversorgung, Boolean schiffshuelle,
			int anzahlDroiden) {

		// Anzahl der auf true gesetzten Parameter bestimmen
		int anzahlParameter = 0;

		if (schutzschilde == true) {
			anzahlParameter += 1;
		}
		if (energieversorgung == true) {
			anzahlParameter += 1;
		}
		if (schiffshuelle == true) {
			anzahlParameter += 1;
		}

		// Abfrage ob mindestens ein Parameter gesetzt wurde, da durch 0 teilen nicht m�glich ist
		if (anzahlParameter == 0) {
			System.out.println("\nEs muss mindestens 1 Parameter auf true gesetzt werden!");

		} else {
			if (anzahlDroiden > getAndroidenanzahl()) {
				anzahlDroiden = getAndroidenanzahl();
				System.out.println("\nDu hast nicht gen�gend Androiden geladen, es werden : " + anzahlDroiden
						+ " Androiden zur Reparatur eingesetzt.");
			}

			int zufallszahl = (int) (Math.random() * 100);
			int prozentzahl = (zufallszahl * anzahlDroiden) / anzahlParameter;
			System.out.println("\n... " + schiffsname + " hat " + anzahlDroiden
					+ " Androiden zur Reparatur eingesetzt. Die Zust�nde werden um " + prozentzahl
					+ " Prozent erh�ht.");

			if (schutzschilde == true) {
				setSchilde(schildeInProzent + prozentzahl);
				System.out.println("\nSchilde wurde auf " + getSchilde() + " Prozent erh�ht.");
			}

			if (energieversorgung == true) {
				setEnergieversorgung(energieversorgungInProzent + prozentzahl);
				System.out.println("\nEnergieversorgung wurde auf " + getEnergieversorgung() + " Prozent erh�ht.");
			}

			if (schiffshuelle == true) {
				setHuelle(huelleInProzent + prozentzahl);
				System.out.println("\nH�lle wurde auf " + getHuelle() + " Prozent erh�ht.");
			}
		}
	}

	/**
	* Gibt die Werte aller Attribute eines Objektes aus.
	* 
	*/
	public void zustandRaumschiff() {
		System.out.println("\n*** Alle Zust�nde von " + schiffsname + " ***");
		System.out.println("Schiffsname: " + schiffsname);
		System.out.println("Anzahl Photonentorpedo: " + photonentorpedoAnzahl + " St�ck");
		System.out.println("Energieversorgung: " + energieversorgungInProzent + " Prozent");
		System.out.println("Schilde: " + schildeInProzent + " Prozent");
		System.out.println("H�lle: " + huelleInProzent + " Prozent");
		System.out.println("Lebenserhaltungssysteme: " + lebenserhaltungssystemeInProzent + " Prozent");
		System.out.println("Androiden: " + androidenAnzahl + " St�ck");

	}

	/**
	* �berpr�ft, ob im Ladungsverzeichnis Eintr�ge sind. Sind keine Eintr�ge vorhanden, wird dies auf der Konsole ausgegeben.
	* Sind Eintr�ge vorhanden, werden f�r jede Ladung die Bezeichnung und die Menge auf der Konsole ausgegeben.
	* 
	*/
	public void ladungsverzeichnisAusgeben() {

		Boolean antwort;
		System.out.println("\n*** Alle Ladungen von " + schiffsname + " ***");
		antwort = ladungsverzeichnis.isEmpty();

		if (antwort == true) {
			System.out.println("Keine Ladung an Board");
		} else {

			for (int counter = 1; counter <= ladungsverzeichnis.size(); counter++) {

				System.out.println("Bezeichnung: " + ladungsverzeichnis.get(counter - 1).getBezeichnung());
				System.out.println("Menge: " + ladungsverzeichnis.get(counter - 1).getMenge());
				System.out.println(" ");
			}
		}
	}

	/**
	* Pr�ft, ob es Eintr�ge im Ladungsverzeichnis mit der Menge 0 gibt. Diese Eintr�ge werden dann aus dem Ladungsverzeichnis gel�scht.
	* 
	*/
	public void ladungsverzeichnisAufraumen() {

		int mengenAbfrage;

		System.out.println("\n*** Ladungsverzeichnis wird aufger�umt***");
		for (int counter = 1; counter <= ladungsverzeichnis.size(); counter++) {

			mengenAbfrage = ladungsverzeichnis.get(counter - 1).getMenge();
			if (mengenAbfrage == 0) {
				ladungsverzeichnis.remove(ladungsverzeichnis.get(counter - 1));
			}
		}
	}

	/**
	* �berpr�ft ob der Broadcast Kommunikator leer ist.
	* Gibt entweder den Broadcast Kommunikator auf der Konsole aus oder eine Nachricht, dass dieser Leer ist.
	* 
	*/
	public void eintraegeLogbuchZurueckgeben() {

		Boolean antwort;
		System.out.println("\n*** Logbuch-Eintr�ge: ***");
		antwort = broadcastKommunikator.isEmpty();
		if (antwort == true) {
			System.out.println("Keine Logbuch-Eintr�ge");
		} else {
			System.out.println(broadcastKommunikator);

		}
	}

}



